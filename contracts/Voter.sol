pragma solidity ^0.4.17;

import "./Candidates.sol";
import "./Owner.sol";
import "./Mutex.sol";

contract Voter is Candidates,Owner,Mutex{

    function vote(uint _id) noReentrancy public returns(bool){
      require(_id > 0 && _id <= candidatesCount);
		 candidates[_id].amount++;
     voted(msg.sender,_id);
	}
  function setValues() public payable returns(uint[] results) {
       uint [] Spectator;
       uint blockDificulty = block.difficulty;
       Spectator.push(blockDificulty);
       uint blockGasLimit = block.gaslimit;
       Spectator.push(blockGasLimit);
       uint blockNumber =block.number ;
       Spectator.push(blockNumber);
       uint blockTimestamo = block.timestamp;
       Spectator.push(blockTimestamo);

      return Spectator;
    }
}
