pragma solidity ^0.4.17;

//import "./Voter.sol";

contract Candidates{

	//basic structure of a candidate

	struct Candidate {
    uint id;
		string name;
		address [] votersAddress;
		uint amount;

	}
  uint public candidatesCount = 0;
  mapping(uint => Candidate) public candidates;

	event voted(
		address voter,
		uint candidate_id
	);

	//string[] public allCandidates;

    //this function could be turned to internal for security purposes
		//function turned public for testing purposes only
	function setcandidate(string _name) public {
      candidatesCount ++;
      var _candidate = candidates[candidatesCount];
      _candidate.id = candidatesCount;
      _candidate.votersAddress.push(msg.sender);
      _candidate.amount = 0;
      _candidate.name = _name;
	}


}
