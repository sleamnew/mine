pragma solidity ^0.4.17;
contract Owner {
  address public ownersAddress;
  /// depsnding on the detailed design the below method sld be public but i think we need to change the detailed design
  function Owner() internal {
    ownersAddress = msg.sender;
  }
  modifier onlyOwner(){
    require(msg.sender == ownersAddress);
    _;
  }

}
//defines a pre conditon for the function
