pragma solidity ^0.4.17;
//
//:-NB: this smart contract has been writen according to the detailed design of the sds
//

import "./Spectator.sol";
import "./Voter.sol";
contract Election is Voter,Spectator {


  //the below code is used for the emergency stop / CIRCUIT BREAKER pattern in the security paterns group which also implements the access AccessRestriction pattern

  bool isVotingGoingOn = true;
  //address owner;
  modifier votingcondition(bool _condition){
    require(_condition); _;
  }

function stopVotingProcess()onlyOwner public{
  isVotingGoingOn = false;
}

    	function Election() public votingcondition(isVotingGoingOn) {
      //  setOwner();
        setcandidate("eprdf");
	      setcandidate("ethiopia");
        setcandidate("ginbot 7");
	       //countCandidates();
    	}

}
