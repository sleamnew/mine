pragma solidity ^0.4.17;
contract Mutex {
  bool locked;

  modifier noReentrancy() {
    require(!locked);
    locked = true;
    _;
    locked = false;
  }

}

//deadlock avoid
//holds  the lock when executing a function then sets it false when done executing
