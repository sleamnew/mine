App = {
  web3Provider: null,
  contracts: {},
  account: '0x0',

  init: function() {
    return App.initWeb3();
  },


  initWeb3: function() {
    // TODO: refactor conditional
    if (typeof web3 !== 'undefined') {
      // If a web3 instance is already provided by Meta Mask.
      App.web3Provider = web3.currentProvider;
      web3 = new Web3(web3.currentProvider);
    } else {
      // Specify default instance if no web3 instance provided
      App.web3Provider = new Web3.providers.HttpProvider('http://localhost:7545');
      web3 = new Web3(App.web3Provider);
    }
    return App.initContract();
  },

  initContract: function() {
    $.getJSON("Election.json", function(election) {
      // Instantiate a new truffle contract from the artifact
      App.contracts.Election = TruffleContract(election);
      // Connect provider to interact with contract
      App.contracts.Election.setProvider(App.web3Provider);
      App.listenForEvents();
      return App.render();
    });
  },

  render: function() {
    var electionInstance;
    //var candidatesCount;
    //web3.eth.defaultAccount = acc[0];
    var loader = $("#loader");
    var content = $("#content");

    loader.show();
    content.hide();

    //web3.eth.getAccounts().then(function(instance){ app=instance })
    //web3.eth.getCoinbase
    // Load account data
    //ethereum.enable();
    var candidatesResults = $("#candidatesResults");
    var resultsArray;
    web3.eth.getBlockNumber((err,res)=>{

    //  console.log(res);
      web3.eth.getBlock(res,(err,result)=>{
        resultsArray = result;
        candidatesResults.empty();
        var candidateTemplate = "<tr><th>" +resultsArray['number'] + "</th><td>" + resultsArray['timestamp'] + "</td><td>" + resultsArray['transactions'] + "</td></tr>"
        candidatesResults.append(candidateTemplate);
      //  console.log(resultsArray);
      })

    })

    web3.eth.getCoinbase((err, res) => {
      if (err === null) {
        App.account = res;
        //$("#account").html("Your account: " + res);
        $("#accountAddress").html("Your Account: " + App.account);
        //console.log(App.account);

        //return App.balanceOf();
      }
    });
    // Load contract data
  App.contracts.Election.deployed().then(function(instance) {
      electionInstance = instance;
      return electionInstance.candidatesCount();
    }).then(function(candidatesCount) {
      minecandidatesCount = 0;
      var minecandidatesCount = (candidatesCount + 0)/10;
      console.log(minecandidatesCount);
      var candidatesSelect = $('#candidatesSelect');
      var test = $('#test');
      var names = [];
      var amounts = [];
      candidatesSelect.empty();
      test.empty();
      //console.log(candidatesCount);
      for (var i = 1; i <= minecandidatesCount; i++) {
        //console.log(candidatesCount);
        electionInstance.candidates(i).then(function(candidate) {
          var id = candidate[0];
          var name = candidate[1];

          var amount = candidate[2];

          // Render candidate Result
          //var candidateTemplate = "<tr><th>" + id + "</th><td>" + name + "</td><td>" + amount + "</td></tr>"
          //candidatesResults.append(candidateTemplate);
          //
          var cardTest = " <div class='card col' style='margin:1%;'> <img class='card-img-top' src='"+name+".jpg' alt='Card image cap'><div class='card-body'><h5 class='card-title'> "+name+"</h5><p > votes : "+amount+"</p><span> id: "+id+"</span></div></div>"
          // Render candidate ballot option
          var candidateOption = "<option value='" + id + "' >" + name + "</ option>"
          //console.log(candidateOption);
          //console.log(candidatesSelect);
          test.append(cardTest);
          //names.append(name);
          //amounts.append(amount);
          candidatesSelect.append(candidateOption);
          //console.log(candidatesSelect.append(candidateOption));
        });
      }

      loader.hide();
      content.show();
    }).catch(function(error) {
      console.warn(error);
    });
  },
    listenForEvents : function(){
      App.contracts.Election.deployed().then(function(instance){
        instance.voted({},{
          fromBlock:'latest',
          toBlock:'latest',}).watch(function(error,event){
            //console.log("event triggered" , event);
            //fix this
          //  $("#content").hide();
          //  $("#loader").show();
          //location.reload(true);
          //App.render();
          //can we call reload from the code
           //$("#content").show();
          //$("#loader").hide();
        //location.reload(true);
          });
      });
    },
    Vote: function() {
      var _id = $('#candidatesSelect').val();
      App.contracts.Election.deployed().then(function(instance){
        //console.log(instance.vote(_id));
        $("#content").hide();
        $("#loader").show();
        return instance.vote(_id);
      }).then(function(result){
        // $("#content").hide();
        // $("#loader").show();
      //console.log("reloaded");
      location.reload(true);
        //content.hide();
        //loader.show();
      }).catch(function(err){
        //console.log(err);
        location.reload(true);
        console.warn(err);
      });
    },
    Spectate : function(){
      var candidatesResults = $("#candidatesResults");
      App.contracts.Election.deployed().then(function(instance){
        var values = instance.setValues();
        console.log(values);
        candidatesResults.empty();
        var candidateTemplate = "<tr><th>" + values[0] + "</th><td>" + values[1] + "</td><td>" + values[2] + "</td></tr>"
        candidatesResults.append(candidateTemplate);
        //
      });
    }
};

$(function() {
  $(window).load(function() {
    App.init();
  });
});
