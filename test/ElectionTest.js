
const Election = artifacts.require('./Election.sol')
const assert = require("chai").assert;
const truffleAssert = require('truffle-assertions');

let electionInstance;

contract('Election' , (accounts)  => {
   beforeEach(async () => {
    electionInstance = await Election.deployed()
   })

     it('initial votes casted for candidate id 1 is zero',async()=>{
       const votesCasted = await electionInstance.getammount(1);
       assert.equal(votesCasted , 0);
     })
     it('initial votes casted for candidate id 2 is zero',async()=>{
       const votesCasted = await electionInstance.getammount(2);
       assert.equal(votesCasted , 0);
     })
     it('initial votes casted for candidate id 3 is zero',async()=>{
       const votesCasted = await electionInstance.getammount(3);
       assert.equal(votesCasted , 0);
     })

     it('should vote for a candidate with id 1' , async() => {
        await electionInstance.vote(1);

        const total = await electionInstance.getammount(1);
        assert.equal(total , 1);
   })
   it('should vote for a candidate with id 2' , async() => {
      await electionInstance.vote(2);

      const total = await electionInstance.getammount(2);
      assert.equal(total , 1);
   })
   it('should vote for a candidate with id 3' , async() => {
      await electionInstance.vote(3);

      const total = await electionInstance.getammount(3);
      assert.equal(total , 1);
  })
  it('should Set a candidate' , async() => {
     await electionInstance.setcandidate("test");

     const total = await electionInstance.candidatesCount();
     assert.equal(total , 4);
   })
   it('should emit an event for candidate id 1' , async() => {
      let tx = await electionInstance.vote("1");

      truffleAssert.eventEmitted(tx,'voted');
  })
  it('should emit an event for candidate id 2' , async() => {
     let tx = await electionInstance.vote("2");

     truffleAssert.eventEmitted(tx,'voted');
  })
  it('should emit an event for candidate id 3' , async() => {
     let tx = await electionInstance.vote("3");

     truffleAssert.eventEmitted(tx,'voted');
  })


   it("should not be able to vote for id less than 1", async () => {
       await truffleAssert.reverts(electionInstance.vote(0));
   });
   it("should not be able to vote for id greater than number of candidates", async () => {
     let candidatesCount = await electionInstance.candidatesCount();
       await truffleAssert.reverts(electionInstance.vote(candidatesCount+1));
   });
   it("the owner of the contract should execute the stopVotingProcess method", async () => {
     await truffleAssert.passes(
      electionInstance.stopVotingProcess());
   });
   it("should not be able to execute the stopVotingProcess function from account 1", async () => {

       await truffleAssert.reverts(
           electionInstance.stopVotingProcess({from: accounts[1]}));
   });
   it("should not be able to execute the stopVotingProcess function from account 2", async () => {

       await truffleAssert.reverts(
           electionInstance.stopVotingProcess({from: accounts[2]}));
   });
   it("should not be able to execute the stopVotingProcess function from account 3", async () => {

       await truffleAssert.reverts(
           electionInstance.stopVotingProcess({from: accounts[3]}));
   });
   it("should not be able to execute the stopVotingProcess function from account 4", async () => {

       await truffleAssert.reverts(
           electionInstance.stopVotingProcess({from: accounts[4]}));
   });
   it("should not be able to execute the stopVotingProcess function from account 5", async () => {

       await truffleAssert.reverts(
           electionInstance.stopVotingProcess({from: accounts[5]}));
   });
   it("should not be able to execute the stopVotingProcess function from account 6", async () => {

       await truffleAssert.reverts(
           electionInstance.stopVotingProcess({from: accounts[6]}));
   });
   it("should not be able to execute the stopVotingProcess function from account 7", async () => {

       await truffleAssert.reverts(
           electionInstance.stopVotingProcess({from: accounts[7]}));
   });
   it("should not be able to execute the stopVotingProcess function from account 8", async () => {

       await truffleAssert.reverts(
           electionInstance.stopVotingProcess({from: accounts[8]}));
   });
   it("should not be able to execute the stopVotingProcess function from account 9", async () => {

       await truffleAssert.reverts(
           electionInstance.stopVotingProcess({from: accounts[9]}));
   });

   it("the getammount function is working for candidate id 1", async () => {
      var result =  await electionInstance.getammount(1);
      assert.equal(result, 2);
   });
   it("the getammount function is working for candidate id 2", async () => {
      var result =  await electionInstance.getammount(2);
      assert.equal(result, 2);
   });
   it("the getammount function is working for candidate id 3", async () => {
      var result =  await electionInstance.getammount(3);
      assert.equal(result,2);
   });




    // truffleAssert.eventNotEmitted(tx, 'Payout');

})
